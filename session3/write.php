<?php
include("../lib/header.php");
?>
<?php
$post = isset($_GET["post"]) ? $_GET["post"] : '';
$board = isset($_GET["board"]) ? $_GET["board"] : '';
$mode = empty($post)? 'write' : 'edit';

if($mode==='edit'):
    $sql_post_info = "SELECT * FROM Posts WHERE no = '$post';";

    $result_post_info = mysqli_query($conn, $sql_post_info);
    if(mysqli_num_rows($result_post_info)>0):
        $post_info = mysqli_fetch_array($result_post_info);
        $board = $post_info["board_no"];
    endif;
else:
    if(empty($board)):
        echo 'Need a Board Number';
        exit();
    endif;
endif;
?>
<style>
.view th{
    width:100px;
    background-color:#eee;
}
.view td{
    text-align:left;
}
.view th,
.view td{
    padding:0.25rem 0.5rem;
}
.view td.content{
    min-height:300px;
    vertical-align: top;
}
.view td.content textarea{
    width:100%;
}
</style>
<form action="write_proc.php" method="POST">
    <input type="hidden" name="board" value="<?php echo $board;?>"/>
    <input type="hidden" name="mode" value="<?php echo $mode;?>"/>
    <table class="view">
        <tbody>
            <tr>
                <th>제목</th>
                <td><input type="text" name="title" value="<?php if($mode==='edit') echo $post_info['title'];?>"/></td>
            </tr>
            <tr>
                <th>작성자</th>
                <td><input type="text" name="author" value="<?php if($mode==='edit') echo $post_info['author'];?>"/></td>
            </tr>
            <tr>
                <td class="content" colspan="2">
                <textarea name="content"><?php if($mode==='edit') echo $post_info['content'];?></textarea>
                </td>
            </tr>
        <tbody>
    </table>
    <button type="button" onclick="history.back(-1)">Previous</button>
    <button type="submit">Save</button>
</form>
<?php
include("../lib/footer.php");
?>