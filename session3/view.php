<?php
include("../lib/header.php");
?>
<?php
$post = isset($_GET["post"]) ? $_GET["post"] : '';

if(!empty($post)):
    $sql_post_info = "SELECT * FROM Posts WHERE no = '$post';";

    $result_post_info = mysqli_query($conn, $sql_post_info);
    if(mysqli_num_rows($result_post_info)>0):
        $post_info = mysqli_fetch_array($result_post_info);
?>
<style>
.view th{
    width:100px;
    background-color:#eee;
}
.view td{
    text-align:left;
}
.view th,
.view td{
    padding:0.25rem 0.5rem;
}
.view td.content{
    min-height:300px;
    vertical-align: top;
}
</style>
<table class="view">
    <tbody>
        <tr>
            <th>제목</th>
            <td><?php echo $post_info["title"];?></td>
        </tr>
        <tr>
            <th>작성자</th>
            <td><?php echo $post_info["author"];?></td>
        </tr>
        <tr>
            <th>작성일</th>
            <td><?php echo $post_info["regdate"];?></td>
        </tr>
        <tr>
            <td class="content" colspan="2"><?php echo $post_info["content"];?></td>
        </tr>
    <tbody>
</table>
<button type="button" onclick="location.href='list.php?board=<?php echo $post_info["board_no"];?>'">LIST</button>
<button type="button" onclick="location.href='write.php?post=<?php echo $post_info["no"];?>'">EDIT</button>
<?php
    else:
        echo "Posts Not Found";
    endif;
else:
    echo "No Post Number";
endif;
include("../lib/footer.php");
?>