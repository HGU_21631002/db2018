# 게시판 만들기 [Create Board] (2) 

## 과정 [Process]
1. 관계형 DB 설계 [Relational DB Design] - Ver.2
2. 웹게시판 [Web Board]- Ver.2
3. 기본기능과 2번째 고도화 [Basic Functions and the 2nd Enhancement] - Ver.2

### 1. 관계형 DB 설계 [Relational DB Design] - Ver.2
DBMS를 엑셀(Excel)에 비유하면 Database는 파일(.xls File), Table은 시트(Sheet)이다.<br/>
게시판을 추가 생성하기 위해서는 새로운 Database를 만들거나, 새로운 Table을 만드는 방법이 존재한다. 그러나 이 방법들은 데이터의 파편화를 일으키므로 우리가 사용하는 RDBMS의 취지에는 적합하지않다.<br/>
우리는 한 파일에, 그리고 한 시트에 데이터를 저장하여 관리하고 싶다. 그러기 위해서는 관계(Relation)를 활용해야한다.

#### 설계조건 [Condition] - Ver.2

- 게시물을 관리하는 테이블 [Table managing Posts]
- 게시판을 관리하는 테이블 [Table managing Boards]
- 게시판과 게시물의 관계 설정 [Setting up Relation]

#### 스키마 만들기 [Creating a Schema] - Ver.2

`CREATE TABLE boards (
  no INT(11) unsigned NOT NULL AUTO_INCREMENT,
  title VARCHAR(100) NOT NULL,
  regdate DATETIME NOT NULL,
  PRIMARY KEY (no)
);`

`CREATE TABLE posts (
  no INT(11) unsigned NOT NULL AUTO_INCREMENT,
  title VARCHAR(100) NOT NULL,
  author VARCHAR(100) NOT NULL,
  content TEXT,
  regdate DATETIME NOT NULL,
  PRIMARY KEY (no)
);`

#### 샘플 데이터 입력 [Input sample data] - Ver.2

`INSERT INTO Boards (title, regdate) VALUES('NOTICE', now());`<br/>
`INSERT INTO Boards (title, regdate) VALUES('HOMEWORK', now());`

`INSERT INTO Posts (title, author, content, regdate) VALUES('first notice', 'admin','none contents', now());`<br/>
`INSERT INTO Posts (title, author, content, regdate) VALUES('second notice', 'admin','none', now());`<br/>
`INSERT INTO Posts (title, author, content, regdate) VALUES('third notice', 'admin','contents', now());`

#### 관계 설정 [Setting up Relation] - Ver.2

관계는 1:1, 1:N, N:M 관계가 존재한다.<br/>
게시판과 게시물의 관계는 한 게시판이 여러개의 게시물을 관계하므로 1:N의 관계이다.

![erd-01](erd-01.png)

*ERD 표기법에 따른 1:N 관계*

이러한 관계를 생성하는 방법으로는 2가지 방법을 추천하는데, 1번째는 Posts와 Boards를 연결하는 새로운 Table을 생성하는 방법이 있고[1], 2번째 방법으로는 Posts에 해당 게시판을 연결하는 attribute(board_no)를 집어넣는 방법이 있다[2]. 어떤것이 더 좋다고는 당장 말하기는 힘드므로, 자신의 개발 취향에 따른다고만 알아두자.

![crowfoot-01](crowfoot-01.png)

*Crow-feet 표기법에 따른 관계 [1]*

![crowfoot-02](crowfoot-02.png)

*Crow-feet 표기법에 따른 관계 [2]*

우리는 [2]번 방법에 따르기로 하고, 앞서 생성한 스키마를 수정하도록 하자.<br/>
스키마의 수정을 하기 위해서는 `ALTER` 명령어를 사용해야 한다.

`ALTER TABLE Posts ADD board_no INT(11) UNSIGNED NOT NULL;`<br/>
`UPDATE Posts SET board_no = 1;`<br/>
`ALTER TABLE Posts ADD FOREIGN KEY ( board_no ) REFERENCES Boards(no) ON DELETE CASCADE;`

Posts 테이블에 `board_no` attribute를 추가하고, 이 `board_no`와 Boards 테이블의 `no`의 관계 (Foreign Key)를 지정해주면 앞으로 두 테이블간의 관계가 유지되게 된다.<br/>

관계가 제대로 설정되어있다면, 다음 명령어를 입력했을때 오류가 발생할 것이다.<br/>
`UPDATE Posts SET board_no = 3;`

이제 다음 쿼리를 입력하여, 게시물을 더 추가하도록 하자.

`INSERT INTO Posts (title, author, content, board_no, regdate) VALUES('first HW', 'admin','none contents', '2', now());`<br/>
`INSERT INTO Posts (title, author, content, board_no, regdate) VALUES('second HW', 'admin','none', '2', now());`<br/>
`INSERT INTO Posts (title, author, content, board_no, regdate) VALUES('third HW', 'admin','contents', '2', now());`

### 2. 웹게시판 [Web Board] - Ver.2

지난 `Session 2`에 이어 이번에도 마찬가지로, [session3/index.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session3/index.php)를 다음의 경로에 복사하도록 하자.

* [HTDOCS]/db2018
    - /session3
        + index.php

모든 과정이 끝났으면, [http://localhost:[포트번호]/db2018/session3/index.php](http://localhost/db2018/session3/index.php)를 브라우져에서 확인한다.(포트번호를 유의하자)<br/>

![ver2](ver2.png)

[session3/index.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session3/index.php)의 소스코드를 확인해보자.

[LINE 5-6]<br/>
`$board = isset($_GET["board"]) ? $_GET["board"] : '1';`<br/>
`$sql_board_info = "SELECT * FROM Boards WHERE no = '$board';";`

위 코드는 브라우져의 URL `http://localhost/db2018/session3/index.php`에 별다른 `GET QUERY`가 없으면 $board 변수에는 1이 할당되어<br/>
$sql_board_info = `SELECT * FROM Boards WHERE no = '1';`;라는 쿼리를 실행하게 된다.<br/>

만약 `http://localhost/db2018/session3/index.php?board=2`라는 식으로 $board 변수에 2가 할당되면 `SELECT * FROM Boards WHERE no = '2';`가 실행되는 구조이다.

이를 통해 게시판 프로그램에서 다른 게시판을 선택할 수 있는 기능이 완성된다.

[LINE 29-36]<br/>
`$sql_posts = "SELECT * FROM Posts WHERE board_no = '$board'";`<br/>
<br/>
`$search_type = isset($_GET["search_type"]) ? $_GET["search_type"] : '';`<br/>
`if(!empty($search_type)):`<br/>
&emsp;`$search_keyword = isset($_GET["search_keyword"]) ? $_GET["search_keyword"] : trim('');`<br/>
<br/>
&emsp;`if($search_type === 'author' && !empty($search_keyword)):`<br/>
&emsp;&emsp;`$sql_posts .= " AND $search_type = '$search_keyword'";`<br/>
&emsp;`else:`<br/>
&emsp;&emsp;`$sql_posts .= " AND $search_type LIKE '%$search_keyword%'";`<br/>
&emsp;`endif;`<br/>
`endif;`

위 코드는 게시물의 검색과 관련된 코드이다.<br/>
URL에 `search_type`나 `search_keyword` QUERY가 존재하면 해당 SQL을 추가하게 된다.<br/>

`author`의 경우에는 SQL의 WHERE절에서 `author = '$search_keyword'` 실행하게 되는데, 이는 Posts 테이블에서 author 값이 $search_keyword인 레코드들을 선택하는 것이다.<br/>

이외의 경우에는 SQL의 WHERE절에서 `$search_type LIKE '%$search_keyword%'` 실행하게 되는데, 여기서 `%`는 문자열 패턴에서 `0개 이상의 아무 문자`를 의미하므로 $search_keyword 포함한 모든 문자열을 가지고 있는 레코드들을 선택하는 것이다.<br/>

[LINE 59-67]<br/>
`<form method="GET">`<br/>
&emsp;`<input type="hidden" name="board" value="<?php echo $board;?>"/>`<br/>
&emsp;`<select name="search_type">`<br/>
&emsp;&emsp;`<option value="author" <?php if($search_type==="author") echo "selected='selected'"; ?>>작성자</option>`<br/>
&emsp;&emsp;`<option value="title" <?php if($search_type==="title") echo "selected='selected'"; ?>>제목</option>`<br/>
&emsp;&emsp;`<option value="content" <?php if($search_type==="content") echo "selected='selected'"; ?>>내용</option>`<br/>
&emsp;`</select>`<br/>
&emsp;`<input type="text" name="search_keyword" />`<br/>
&emsp;`<button type="submit">SEARCH</button>`<br/>
`</form>`

위 코드는 게시물의 검색폼이다.<br/>
`<form>` 태그의 `action`이 정해지지 않았으므로 현재페이지를 타겟으로 잡고 `method`가 `GET`인 이유는 URL에 GET QUERY를 요청하기 위해서이다. 지난 `Session 2`를 참고하자.<br/>
`name`이 board인 `<input>` 태그는 `type`이 hidden이다. hidden 타입은 화면에는 보이지 않지만 `<form>` 태그 안에서 확실히 존재한다. 그리고 현재 value는 $board의 번호이다.<br/>
`<select>` 태그의 `name`은 search_type이다. 아래 `<option>`들 중에서 선택된 value를 search_type의 value가 된다.<br/>
`name`이 search_keyword인 `<input>` 태그에 입력한 value가 search_keyword의 value가 된다.<br/>
`<button>` 태그의 `type`은 submit이다. 버튼을 클릭하면 `<form>` 태그 내의 모든 name과 value가 `GET`방식으로 wrapping 되어 URL 패러미터로 재생성되어 `action(정해주지 않으면 현재페이지)`을 실행한다.

### 3. 기본기능과 2번째 고도화 [Basic Functions and the 2nd Enhancement] - Ver.2

#### 기본 기능 [Basic Functions]
- 목록 [List] [session3/list.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session3/list.php)
- 읽기 [VIEW] [session3/view.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session3/view.php)
- 쓰기(편집) [WRITE(EDIT)] [session3/write.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session3/write.php)
- 처리 [Process] [session3/write_proc.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session3/write_proc.php)

#### 2번째 고도화 - 토의 [2nd Enhancement - Discussion]
- 답글/코멘트 [Reply/Comment]
- 권한 [Authority]
