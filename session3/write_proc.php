<?php
if(empty($_POST['mode'])):
    echo '<script>alert("need mode selection");history.back();</script>';
    exit();
endif;

if(empty($_POST['title'])):
    echo '<script>alert("need title");history.back();</script>';
    exit();
endif;

if(empty($_POST['author'])):
    echo '<script>alert("need author");history.back();</script>';
    exit();
endif;

include("../lib/dbHandler.php");

if($_POST['mode']==='write'):
    if(empty($_POST['board'])):
        echo '<script>alert("need board number");history.back();</script>';
        exit();
    endif;
    
    $sql_write = "INSERT INTO Posts (title, author, content, board_no, regdate) ".
        "VALUES ('".$_POST['title']."','".$_POST['author']."', '".$_POST['content']."', '".$_POST["board"]."', now());";
    if(!mysqli_query($conn, $sql_write)):
    echo "Error: " . $sql_write . "<br>" . mysqli_error($conn);
    endif;

elseif($_POST['mode']==='edit'):
    if(empty($_POST['post'])):
        echo '<script>alert("need post number");history.back();</script>';
        exit();
    endif;

    if(empty($_POST['board'])):
        echo '<script>alert("need board number");history.back();</script>';
        exit();
    endif;

    $sql_edit = "UPDATE Posts SET title = '".$_POST['title']."', content = '".$_POST['content']."' WHERE no = '".$_POST["post"]."';";
    if(!mysqli_query($conn, $sql_edit)):
    echo "Error: " . $sql_edit . "<br>" . mysqli_error($conn);
    endif;

endif;


if($conn):
    mysqli_close($conn);
endif;
echo "<script>location.href='list.php?board=".$_POST['board']."'</script>";
exit();
?>