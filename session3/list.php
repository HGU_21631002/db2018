<?php
include("../lib/header.php");
?>
<?php
$board = isset($_GET["board"]) ? $_GET["board"] : '1';
$sql_board_info = "SELECT * FROM Boards WHERE no = '$board';";

$result_board_info = mysqli_query($conn, $sql_board_info);
if(mysqli_num_rows($result_board_info)>0):
    $board_info = mysqli_fetch_array($result_board_info);
?>
<table>
    <caption><?php echo $board_info["title"];?></caption>
    <thead>
        <tr>
            <th>NO</th>
            <th>TITLE</th>
            <th>AUTHOR</th>
            <th>DATE</th>
        </tr>
    </thead>
    <tbody>
<?php
$sql_posts = "SELECT * FROM Posts WHERE board_no = '$board'";

$search_type = isset($_GET["search_type"]) ? $_GET["search_type"] : '';
if(!empty($search_type)):
    $search_keyword = isset($_GET["search_keyword"]) ? $_GET["search_keyword"] : trim('');

    if($search_type === 'author' && !empty($search_keyword)):
        $sql_posts .= " AND $search_type = '$search_keyword'";
    else:
        $sql_posts .= " AND $search_type LIKE '%$search_keyword%'";
    endif;
endif;

$order_col = isset($_GET["order_col"]) ? $_GET["order_col"] : 'no';
$order_type = isset($_GET["order_type"]) ? $_GET["order_type"] : 'DESC';
$sql_posts .= " ORDER BY $order_col $order_type";
$sql_posts .= ";";

$results_posts = mysqli_query($conn, $sql_posts);
while($row = mysqli_fetch_array($results_posts)):
?>
        <tr>
            <td><?php echo $row["no"];?></td>
            <td><a href="view.php?post=<?php echo $row["no"];?>"><?php echo $row["title"];?></a></td>
            <td><?php echo $row["author"];?></td>
            <td><?php echo $row["regdate"];?></td>
        </tr>
<?php
endwhile;
?>
    </tbody>
</table>

<form method="GET">
    <input type="hidden" name="board" value="<?php echo $board;?>"/>
    <select name="search_type">
        <option value="author" <?php if($search_type==="author") echo "selected='selected'"; ?>>작성자</option>
        <option value="title" <?php if($search_type==="title") echo "selected='selected'"; ?>>제목</option>
        <option value="content" <?php if($search_type==="content") echo "selected='selected'"; ?>>내용</option>
    </select>
    <input type="text" name="search_keyword" />
    <button type="submit">SEARCH</button>
</form>

<label><b>SQL COMMAND</b></label>
<div><i><?php echo $sql_posts;?></i></div>

<?php
endif;
include("../lib/footer.php");
?>