# DB 디자인 연습 [DB Design Exercise]
  
  
## 과정 [Process]

1. Designing a Bulletin Board DB
2. Discuss in Group
  
### 1. Designing a Bulletin Board DB

다음과 같은 DB ERD을 MySQLWorkBench를 이용하여 그리고, MySQL에 새로운 데이터베이스 [db_exercise]에 해당 테이블들을 생성하자.

![sample](sample.png)

다음 DML을 통해서 샘플 데이터를 입력하자.

INSERT INTO \`db_exercise\`.\`groups\` (\`level\`, \`name\`) VALUES ('1', 'administrator');  
INSERT INTO \`db_exercise\`.\`groups\` (\`level\`, \`name\`) VALUES ('2', 'member');  
INSERT INTO \`db_exercise\`.\`groups\` (\`level\`, \`name\`) VALUES ('3', 'guest');  

INSERT INTO \`db_exercise\`.\`boards\` (\`name\`, \`auth_admin\`, \`auth_write\`, \`auth_read\`) VALUES ('notice', '1', '3', '3');  
INSERT INTO \`db_exercise\`.\`boards\` (\`name\`, \`auth_admin\`, \`auth_write\`, \`auth_read\`) VALUES ('board1', '1', '3', '3');  
INSERT INTO \`db_exercise\`.\`boards\` (\`name\`, \`auth_admin\`, \`auth_write\`, \`auth_read\`) VALUES ('board2', '1', '2', '3');  

INSERT INTO \`db_exercise\`.\`users\` (\`name\`, \`user_id\`, \`group_no\`) VALUES ('kunlee', 'kunlee', '1');  
INSERT INTO \`db_exercise\`.\`users\` (\`name\`, \`user_id\`, \`group_no\`) VALUES ('member1', 'member1', '2');  
INSERT INTO \`db_exercise\`.\`users\` (\`name\`, \`user_id\`, \`group_no\`) VALUES ('member2', 'member2', '2');  
INSERT INTO \`db_exercise\`.\`users\` (\`name\`, \`user_id\`, \`group_no\`) VALUES ('guest1', 'guest1', '3');  
INSERT INTO \`db_exercise\`.\`users\` (\`name\`, \`user_id\`, \`group_no\`) VALUES ('guest2', 'guest2', '3');  

INSERT INTO \`db_exercise\`.\`documents\` (\`title\`, \`content\`, \`user_no\`, \`board_no\`) VALUES ('notice1', 'hello', '1', '1');  
INSERT INTO \`db_exercise\`.\`documents\` (\`title\`, \`content\`, \`user_no\`, \`board_no\`) VALUES ('notice2', 'hello', '1', '1');  
INSERT INTO \`db_exercise\`.\`documents\` (\`title\`, \`content\`, \`user_no\`, \`board_no\`) VALUES ('hi1', 'hello1', '2', '2');  
INSERT INTO \`db_exercise\`.\`documents\` (\`title\`, \`content\`, \`user_no\`, \`board_no\`) VALUES ('hi2', 'hello2', '2', '2');  
INSERT INTO \`db_exercise\`.\`documents\` (\`title\`, \`content\`, \`user_no\`, \`board_no\`) VALUES ('god is good', 'world', '3', '2');  
INSERT INTO \`db_exercise\`.\`documents\` (\`title\`, \`content\`, \`user_no\`, \`board_no\`) VALUES ('all the time', 'world', '3', '2');  
INSERT INTO \`db_exercise\`.\`documents\` (\`title\`, \`content\`, \`user_no\`, \`board_no\`) VALUES ('why not change the world', 'hi all', '4', '2');  
INSERT INTO \`db_exercise\`.\`documents\` (\`title\`, \`content\`, \`user_no\`, \`board_no\`) VALUES ('thank you', 'it\'s my pleasure', '5', '2');  

INSERT INTO \`db_exercise\`.\`comments\` (\`comment\`, \`doc_no\`, \`user_no\`) VALUES ('first', '1', '1');  
INSERT INTO \`db_exercise\`.\`comments\` (\`comment\`, \`doc_no\`, \`user_no\`) VALUES ('second', '1', '2');  
INSERT INTO \`db_exercise\`.\`comments\` (\`comment\`, \`doc_no\`, \`user_no\`) VALUES ('first', '2', '3');  
INSERT INTO \`db_exercise\`.\`comments\` (\`comment\`, \`doc_no\`, \`user_no\`) VALUES ('second', '2', '4');  
INSERT INTO \`db_exercise\`.\`comments\` (\`comment\`, \`doc_no\`, \`user_no\`) VALUES ('third', '2', '5');  
INSERT INTO \`db_exercise\`.\`comments\` (\`comment\`, \`doc_no\`, \`user_no\`) VALUES ('1!', '4', '2');  
INSERT INTO \`db_exercise\`.\`comments\` (\`comment\`, \`doc_no\`, \`user_no\`) VALUES ('2!', '4', '1');  

INSERT INTO \`db_exercise\`.\`attached\` (\`filename\`, \`doc_no\`) VALUES ('notice.docx', '1');  
INSERT INTO \`db_exercise\`.\`attached\` (\`filename\`, \`doc_no\`) VALUES ('homework.txt', '1');  
INSERT INTO \`db_exercise\`.\`attached\` (\`filename\`, \`doc_no\`) VALUES ('notice2.hwp', '2');  
INSERT INTO \`db_exercise\`.\`attached\` (\`filename\`, \`doc_no\`) VALUES ('practice.sql', '2');  


### 2. Discuss in Group

그룹끼리 토의하여 다음과 같은 쿼리를 만들어보자.

1. notice 게시판의 모든 글의 title과 user의 name
2. board1과 board2 게시판의 모든 글의 title과 user의 name
3. title이 notice1인 게시물의 attached file들
4. title이 notice2인 게시물에 달린 comment와 user의 name
5. board2 게시판에서 쓰기 권한을 가진 user들의 name과 user_id
