# AMP(Apache,MySQL,PHP) INSTALLATION - BITNAMI
![PHP-based Stack](php710.png)

* Windows: https://bitnami.com/stack/wamp

* OSX: https://bitnami.com/stack/mamp

**설치시 입력하라고 하는 password는 반드시 기억해야한다**<br/>
**Addon은 phpMyAdmin만 필요함**<br/>
**설치가 끝나면 http://localhost:8080/ 혹은 http://127.0.0.1:8080/ 으로 접속하여 아래처럼 나오면 성공**


![localhost](localhost.png)



# phpMyAdmin

![bitnami_manager](bitnami_manager.png)

Open phpMyAdmin을 누르거나, 웹브라우져에서 http://localhost:8080/phpmyadmin/ 로 접속하면 mysql에 웹프로그램을 통해 접속이 가능하다.

![phpmyadmin_1](phpmyadmin_1.png)
사용자명은 기본 root이고, 비밀번호는 앞서 bitnami를 설치하면서 지정한 비밀번호를 입력한다.

![phpmyadmin_2](phpmyadmin_2.png)
접속에 성공하면 이렇게 나오게 된다.



# SQL(Structured Query Language)

* DDL(Data Definition Language)
<br/>https://dev.mysql.com/doc/refman/5.7/en/sql-syntax-data-definition.html
* DML(Data Manipulation Langauge)<br/>https://dev.mysql.com/doc/refman/5.7/en/sql-syntax-data-manipulation.html

## 실습 - phpMyAdmin
phpMyAdmin의 상단메뉴중 SQL을 눌러 해당 Query를 입력한다.
![sql_1](sql_1.png)
`CREATE DATABASE DB2018`
<p>위의 쿼리를 입력하고 실행을 누른다.</p>

![sql_2](sql_2.png)
<p>해당 DB가 생성되었음을 확인한다.</p>

![sql_3](sql_3.png)
<p>DB2018을 선택하여 이번에는 테이블을 만들도록 하자. 제대로 선택되었다면 SQL메뉴를 클릭했을때 '데이터베이스 db2018에 SQL 질의를 실행'이라는 메시지를 볼수 있다.</p>

`CREATE TABLE posts_ex1 (
  post_no INT(11) unsigned NOT NULL,
  post_title VARCHAR(100) NOT NULL,
  post_content TEXT,
  PRIMARY KEY (post_no)
);`
<p>위 쿼리를 실행시키자.</p>

![sql_4](sql_4.png)
<p>이렇게 나온다면 성공이다.</p>

## 실습 - CONSOLE

비트나미가 설치된 경로의 mysql/bin으로 들어가면 mysql실행파일이 존재한다.(osx기준)
경로를 모르는 경우에는 비트나미 매니져프로그램에서 Open Application Folder로 들어가면 확인할수 있다.
![bitnami_openfolder](bitnami_openfolder.png)
![mysql_path](mysql_path.png)

<p>터미널(Windows: CMD, OSX: Term 등) 에서 다음 명령어를 입력하자.</p>
![mysql_exec](mysql_exec.png)
`./mysql -uroot -p[패스워드]`
<p><b>[패스워드]</b>부분에 비트나미 설치시에 입력한 패스워드를 기입한다**</p>

`mysql> `
<p>이렇게 나온다면 제대로 접속이 된 것이다.</p>

앞선 phpmyadmin에서와 마찬가지로 쿼리를 입력한다.

`mysql> CREATE DATABASE DB2018_2;`
<p>Query OK 메시지가 뜨면 성공한 것이다. DB2018_2로 이름을 다르게 기입한 이유는 앞서 phpmyadmin으로 이미 DB2018의 데이터베이스가 생성되어있기때문이다.</p>
<p>생성된 데이터베이스들의 목록을 조회할수가 있다. 다음 쿼리를 실행하자.</p>

`mysql> SHOW DATABASES;`
![mysql_databases](mysql_databases.png)

<p>데이터베이스를 사용하기에 앞서 생성된 데이터베이스중 사용할 데이터베이스를 선택해야한다. 이때는 USE 쿼리를 사용한다.</p>

`mysql> USE DB2018_2;`
<p>Database changed 메시지가 뜨면 선택된 데이터베이스가 변경된 것이다. 다시 DB2018 데이터베이스로 변경하자.</p>

`mysql> USE DB2018;`

<p>이번에는 테이블을 생성하자. 다음 쿼리를 실행시키고 Query OK 메시지가 뜨면 성공한 것이다.</p>
`CREATE TABLE posts_ex2 (
  post_no INT(11) unsigned NOT NULL,
  post_title VARCHAR(100) NOT NULL,
  post_content TEXT,
  PRIMARY KEY (post_no)
);`

<p>현재 DB2018 데이터베이스에 생성된 테이블을 확인하려면 다음 쿼리를 실행하도록 한다.</p>
`mysql> SHOW TABLES;`
![mysql_tables](mysql_tables.png)