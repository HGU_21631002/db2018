<?php
include("../lib/header.php");
?>
<table>
    <caption>board_a</caption>
    <thead>
        <tr>
            <th>NO</th>
            <th>TITLE</th>
            <th>AUTHOR</th>
            <th>CONTENT</th>
            <th>DATE</th>
        </tr>
    </thead>
    <tbody>
<?php
$sql_board_a = "SELECT * FROM board_a;";

$results_board_a = mysqli_query($conn, $sql_board_a);
while($row = mysqli_fetch_array($results_board_a)):
?>
        <tr>
            <td><?php echo $row["no"];?></td>
            <td><?php echo $row["title"];?></td>
            <td><?php echo $row["author"];?></td>
            <td><?php echo $row["content"];?></td>
            <td><?php echo $row["regdate"];?></td>
        </tr>
<?php
endwhile;
?>
    </tbody>
</table>
<br/>
<table>
    <caption>board_b</caption>
    <thead>
        <tr>
            <th>NO</th>
            <th>TITLE</th>
            <th>AUTHOR</th>
            <th>CONTENT</th>
            <th>DATE</th>
        </tr>
    </thead>
    <tbody>
<?php
$sql_board_b = "SELECT * FROM board_b;";
$results_board_b = mysqli_query($conn, $sql_board_b);

while($row = mysqli_fetch_array($results_board_b)):
?>
        <tr>
            <td><?php echo $row["no"];?></td>
            <td><?php echo $row["title"];?></td>
            <td><?php echo $row["author"];?></td>
            <td><?php echo $row["content"];?></td>
            <td><?php echo $row["regdate"];?></td>
        </tr>
<?php
endwhile;
?>
    </tbody>
</table>
<?php
include("../lib/footer.php");
?>