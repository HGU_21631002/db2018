<?php
include("../lib/header.php");
?>

<form action="write_proc.php" method="POST">
    <input type="hidden" name="board_name" value="board_a"/>
    <div>
        <label>TITLE</label>
        <input type="text" name="title"/>
    </div>
    <div>
        <label>AUTHOR</label>
        <input type="text" name="author"/>
    </div>
    <div>
        <label>CONTENT</label>
        <textarea name="content"></textarea>
    </div>
    <div>
        <button type="submit">Submit</button>
        <button type="reset">Clear</button>
    </div>
</form>

<?php
include("../lib/footer.php");
?>