# 게시판 만들기 [Create Board] (1) 

## 과정 [Process]
1. DB 설계 [DB Design]
2. PHP-MySQL 연동 [Integration of PHP and MySQL]
3. 고도화 [Enhanced]
### 1. DB 설계 [DB Design]

#### 설계조건 [Condition] - Ver.1
- 게시물에 필요한 내용은 작성자, 제목, 내용, 작성일 [
What We Need To Write a Post: author, title, content, date of registration]
- 2개의 게시판 [Two bulletin boards] (board_a, board_b)

#### 스키마 만들기 [Creating a Schema] - Ver.1

phpMyAdmin 혹은 mysql console을 사용한다. [Using phpMyAdmin or mysql console mode]

`CREATE TABLE board_a (
  no INT(11) unsigned NOT NULL AUTO_INCREMENT,
  title VARCHAR(100) NOT NULL,
  author VARCHAR(100) NOT NULL,
  content TEXT,
  regdate DATETIME NOT NULL,
  PRIMARY KEY (no)
);`

`CREATE TABLE board_b (
  no INT(11) unsigned NOT NULL AUTO_INCREMENT,
  title VARCHAR(100) NOT NULL,
  author VARCHAR(100) NOT NULL,
  content TEXT,
  regdate DATETIME NOT NULL,
  PRIMARY KEY (no)
);`

#### 샘플 데이터 입력 [Input sample data] - Ver.1

`INSERT INTO board_a (title, author, content, regdate) VALUES('HI1', 'user_1', 'HI~~~!', now());`
<br/>
`INSERT INTO board_a (title, author, content, regdate) VALUES('HI2', 'user_1','HI~~~!!', now());`
<br/>
`INSERT INTO board_a (title, author, content, regdate) VALUES('HI3', 'user_2', 'HI~~~!!!', now());`
<br/>
`INSERT INTO board_a (title, author, content, regdate) VALUES('HI4', 'user_2', 'HI~~~!!!!', now());`
<br/>
`INSERT INTO board_a (title, author, content, regdate) VALUES('HI5', 'user_2', 'HI~~~!!!!!', now());`
<br/>
`INSERT INTO board_a (title, author, content, regdate) VALUES('HI6', 'user_3', 'HI~~~!!!!!', now());`
<br/>
`INSERT INTO board_a (title, author, content, regdate) VALUES('HI7', 'user_3', 'HI~~~!!!!', now());`
<br/>
`INSERT INTO board_a (title, author, content, regdate) VALUES('HI8', 'user_4', 'HI~~~!!!', now());`
<br/>
`INSERT INTO board_a (title, author, content, regdate) VALUES('HI9', 'user_2', 'HI~~~!!', now());`
<br/>
`INSERT INTO board_a (title, author, content, regdate) VALUES('HI10', 'user_1', 'HI~~~!', now());`

`INSERT INTO board_b (title, author, content, regdate) VALUES('HELLO1', 'user_1','WORLD~~~!', now());`
<br/>
`INSERT INTO board_b (title, author, content, regdate) VALUES('HELLO2', 'user_1','WORLD~~~!!', now());`
<br/>
`INSERT INTO board_b (title, author, content, regdate) VALUES('HELLO3', 'user_1','WORLD~~~!!!', now());`
<br/>
`INSERT INTO board_b (title, author, content, regdate) VALUES('HELLO4', 'user_2','WORLD~~~!!!!', now());`
<br/>
`INSERT INTO board_b (title, author, content, regdate) VALUES('HELLO5', 'user_3','WORLD~~~!!!!!', now());`
<br/>
`INSERT INTO board_b (title, author, content, regdate) VALUES('HELLO6', 'user_3','WORLD~~~!!!!!', now());`
<br/>
`INSERT INTO board_b (title, author, content, regdate) VALUES('HELLO7', 'user_2','WORLD~~~!!!!', now());`
<br/>
`INSERT INTO board_b (title, author, content, regdate) VALUES('HELLO8', 'user_1','WORLD~~~!!!', now());`
<br/>
`INSERT INTO board_b (title, author, content, regdate) VALUES('HELLO9', 'user_4','WORLD~~~!!', now());`
<br/>
`INSERT INTO board_b (title, author, content, regdate) VALUES('HELLO10', 'user_3','WORLD~~~!', now());`

![board_a](board_a.png)
![board_b](board_b.png)

### 2. PHP-MySQL 연동 [Integration of PHP and MySQL] - Ver.1

gitlab의 다음 경로에 있는 파일들을 다운받는다.

[lib/dbHandler.php](https://gitlab.com/HGU_21631002/db2018/raw/master/lib/dbHandler.php)

[lib/header.php](https://gitlab.com/HGU_21631002/db2018/raw/master/lib/header.php)

[lib/footer.php](https://gitlab.com/HGU_21631002/db2018/raw/master/lib/footer.php)

[session2/index.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session2/index.php)

또한, **dbEnv.php** 라는 파일을 생성하여, 다음과 같은 내용을 저장한다.

`<?php
$host = 'localhost';
$user = 'root';
$pw = '[mysql비밀번호]'; // [ ]은 제거
$dbName = 'db2018';
?>`


이전에 bitnami를 설치하면서 htdocs가 설정된 폴더로 이동한다.

별다른 설정변경이 없었다면, apache2가 설치된 폴더의 하위폴더에 위치해있다.

매니져프로그램의 Open Application Folder를 통해 apache2가 설치된 폴더로 이동할 수 있다.

앞으로 이 경로는 **[HTDOCS]**라고 명명.

![manager](manager.png)
![htdocs](htdocs.png)

[HTDOCS]/db2018 폴더에 lib폴더와 session2라는 폴더를 새로 생성하여 위에서 언급한 파일들을 저장하자.

* [HTDOCS]/db2018
    - /lib
        + dbEnv.php
        + dbHandler.php
        + header.php
        + footer.php
    - /session2
        + index.php

모든 과정이 끝났으면, localhost:[port#]/db2018/session2/index.php 를 열어 잘나오는지 확인한다.
![ver1](ver1.png)

### 3. 고도화 [Enhanced] - Ver.1

- 데이터의 정렬순서 [Sort Order]
- 특정 검색 [Search]
- 게시물 추가 [Create Post]
- 그 외 추가 기능 [Additional features]

#### 정렬순서 [Sort Order]

지금은 데이터가 1번을 시작으로 10번까지 출력되고있다. 보통 게시판들은 생성시간의 역순으로 정렬되어있기 때문에 정렬순서를 바꿀 필요가 있다.
[session2/index.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session2/index.php)의 `$sql_board_?`의 쿼리 부분을 수정하자.

`$sql_board_? = "SELECT * FROM board_? ORDER BY no DESC;";`

`ORDER BY [attribute] ASC/DESC` 명령어 를 통해 정렬 순서를 변경 시킬 수 있다.

#### 검색 [Search]

특정 게시자의 게시물들만을 원한다면 검색을 위한 조건 명령어를 사용할 수 있다.
앞선 방식과 마찬가지로 쿼리를 수정한다.

`$sql_board_? = "SELECT * FROM board_? WHERE author = 'user_1' ORDER BY no DESC;";`

`WHERE [attribute][condition]` 명령어 를 통해 원하는 레코드만을 가져올 수 있다.

`[condition]`으로는 기본적인 `부등호 =, >=, <=, <>` 뿐 아니라 `LIKE 등을 통한 문자열패턴`도 가능하다.


기본적인 사용자 검색과 관련되어 쿼리를 매번 수정하는 것이 아니라, 웹브라우져의 URL 패러미터를 통해 동적으로 변경 가능한 프로그램으로 바꾸어 보도록 한다.

`$author = isset($_GET["author"]) ? $_GET["author"] : '';
$where = " WHERE 1 = 1";
if(!empty($author)):
    $where .= " AND author = '".$author."'";
endif;
$sql_board_? = "SELECT * FROM board_?" . $where. ";";`

#### 게시물 추가 [Create Post]

게시물을 추가하기 위해서 새로운 파일을 추가한다.

[session2/write_a.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session2/write_a.php)

[session2/write_b.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session2/write_b.php)

[session2/write_proc.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session2/write_proc.php)

위 파일들을 다음의 위치에 저장한다.

* HTDOCS/db2018
    - /session2
        + write_a.php
        + write_b.php
        + write_proc.php

우선 write_a.php, write_b.php 안의 `<form>` tag 부분을 확인하자.

`<form>` tag 안에는 `<input>`과 `<textarea>` tag가 있는데, 이 tag 안의 값들을 `action`으로 `method`를 통해 전달한다.
`method`는 [HTTP 1.1](https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.3) 규약에 따라 GET/POST/... 방식이 있으며, `GET`은 앞서 쿼리에 사용되었던 `$_GET`을 의미하고 `POST`는 [write_proc.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session2/write_proc.php)에서 확인가능한 `$_POST`와 관련되어있다.

`<form>`안에 입력된 값들이 `submit`을 통해 [write_proc.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session2/write_proc.php)으로 전달되고,
[write_proc.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session2/write_proc.php)에서는 `<form>`으로 부터 전달받은 값들을 처리한다.

[write_proc.php](https://gitlab.com/HGU_21631002/db2018/raw/master/session2/write_proc.php)에서의 QUERY를 보면 다음과 같다.

`$sql = "INSERT INTO ".$_POST['board_name']." (title, author, content, regdate) VALUES ('".$_POST['title']."','".$_POST['author']."', '".$_POST['content']."', now());";`

`INSERT INTO [table_name] ([attributes]) VALUES ([values]);` 명령어는 기본적인 DML이므로 항상 기억해두자.


#### 추가 기능 [Additional features]

일반적인 게시판들은 위에 언급한 내용보다도 더 다양한 기능들이 존재한다.

- 새 게시판 생성/삭제
- 검색창의 존재
- etc..

팀원들과 어떤 기능들이 있을지 알아보도록 한다.