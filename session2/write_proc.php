<?php
if(empty($_POST['board_name'])):
    echo '<script>alert("need board_name");history.back();</script>';
    exit();
endif;

if(empty($_POST['title'])):
    echo '<script>alert("need title");history.back();</script>';
    exit();
endif;

if(empty($_POST['author'])):
    echo '<script>alert("need author");history.back();</script>';
    exit();
endif;

include("../lib/dbHandler.php");
$sql = "INSERT INTO ".$_POST['board_name']." (title, author, content, regdate) ".
       "VALUES ('".$_POST['title']."','".$_POST['author']."', '".$_POST['content']."', now());";
if(!mysqli_query($conn, $sql)):
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
endif;

if($conn):
    mysqli_close($conn);
endif;
echo "<script>location.href='index.php'</script>";
exit();
?>