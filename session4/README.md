# DB 디자인 [DB Design]
  
  
## 과정 [Process]
1. DB 디자인 툴 [DB Design Tool]
2. 게시판 DB 디자인하기 [Designing a Bulletin Board DB]
3. 토의하기 [Discuss]
  
  
  
### 1. DB 디자인 툴 [DB Design Tool]
  
|![ms-design](ms-design.gif)|![aquerytool](aquerytool.png)|
|:-----:|:-----:|
| Visual DB Tool (MS) | [AQUERY Tool (Web)](http://aquerytool.com/)|
|![workbench-08](workbench-08.png)|![phpmyadmin](phpmyadmin.png)|
| [MySQLWorkbench](https://www.mysql.com/products/workbench/) | phpMyAdmin |
  
DB를 설계할 때 도움을 주는 가시화된 툴로써, 위에 언급한 툴 외에도 여러 툴들이 많으니 직접 찾아보도록 하자. 이번 세션에서는 MySQLWorkbench를 이용하여 앞서 진행했던 세션에서 만들었던 스키마들을 직접 디자인해보도록 한다.
  
  
#### MySQLWorkbench
  
SQL의 개발과, 설계, 유지, 관리를 도와주는 GUI기반의 DB 설계 프로그램으로 멀티 플랫폼(Windows, Linux, OS X)을 지원한다.  
[https://www.mysql.com/products/workbench](https://www.mysql.com/products/workbench/)에서 다운받도록 한다.  
  
  
  
### 2. 게시판 DB 디자인하기 [Designing a Bulletin Board DB]
  
![workbench-01](workbench-01.png)  
[1] 시작화면에서 왼쪽의 Models를 선택하여 +로 New Doucment를 생성한다.

![workbench-02](workbench-02.png)  
[2] Physical Schema의 이름을 db2018로 수정해놓자.

![workbench-03](workbench-03.png)  
[3] Add Diagram을 선택하여 새로운 ERD를 그릴 준비를 한다.

![workbench-04](workbench-04.png)  
[4] 툴 상자에서 New Table을 선택하여 Canvas에 테이블을 생성한다.

![workbench-05](workbench-05.png)  
[5] 테이블 이름과 속성의 이름/타입 등을 지정한다.

![workbench-06](workbench-06.png)  
[6] 게시판을 위한 다른 테이블들(Boards, Posts)도 추가한다.

![workbench-07](workbench-07.png)  
[7] Boards와 Posts의 1:N 관계를 설정하기 위해서 툴 상자에서 1:n non-identifying Relationship을 선택한다.

![workbench-08](workbench-08.png)  
[8] Posts의 FK(Foreign Key)로 Boards의 PK(Primary Key)가 오는 형태이므로, Posts 테이블을 먼저 선택하고 다음으로 Boards 테이블을 클릭한다.

![workbench-09](workbench-09.png)  
[9] 관계과 형성되었다.

![workbench-10](workbench-10.png)  
[10] 만들어진 테이블의 DDL을 얻으려면 해당 테이블을 선택후 마우스 우측클릭 후, Copy SQL to Clipboard를 선택하자.

![workbench-11](workbench-11.png)  
[11] 문서 편집기 등을 통해서 ctrl + v (paste) 하면, 해당 SQL을 확인 수 있다. 이 SQL을 mysql콘솔/phpmyadmin에서 사용하면된다. 참고로 MySQL 문법이므로 Oracle 등의 문법으로 변환시키고 싶다면, [SQLines](http://www.sqlines.com/) 혹은 [SQLines - Online](http://www.sqlines.com/online) 을 이용하는 것을 추천한다.
  
  
### 3. 토의하기 [Discuss]
  
이전 세션시간동안 만들었던 스키마들을 MySQLWorkbench로 옮기는 작업을 끝낸 후,
다음 기능을 집어넣기 위한 모델과 관계들을 디자인해보도록 하자.
  
- 코멘트
- 사용자정보
  
+ 팀과제로 나온 팀게시판 DB도 디자인해보도록 하자.