<?php
include("dbEnv.php");
$conn = mysqli_connect($host, $user, $pw, $dbName);

if(mysqli_connect_errno($conn)){
    echo "DB connection failure: " . mysqli_connect_error();
    exit();
}
?>