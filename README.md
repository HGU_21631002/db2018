# Database Tutor Session

- [SESSION 1 : AMP Setup](https://gitlab.com/HGU_21631002/db2018/tree/master/session1)

- [SESSION 2 : 게시판 만들기 [Create Bulletin Board] - Ver.1](https://gitlab.com/HGU_21631002/db2018/tree/master/session2)

- [SESSION 3 : 게시판 만들기 [Create Bulletin Board] - Ver.2](https://gitlab.com/HGU_21631002/db2018/tree/master/session3)

- [SESSION 4 : DB 디자인 [DB Design]](https://gitlab.com/HGU_21631002/db2018/tree/master/session4)

- [SESSION 5 : DB 디자인 연습 [DB Design Exercise]](https://gitlab.com/HGU_21631002/db2018/tree/master/session5)
